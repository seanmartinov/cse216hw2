package cse216hw2;

import java.util.List;
import java.util.*;

// TODO : a missing interface method must be implemented in this class to make it compile. This must be in terms of volume().
public class Cuboid implements ThreeDShape {

    private final ThreeDPoint[] vertices = new ThreeDPoint[8];
    private double surfaceArea;

    /**
     * Creates a cuboid out of the list of vertices. It is expected that the vertices are provided in
     * the order as shown in the figure given in the homework document (from v0 to v7).
     * 
     * @param vertices the specified list of vertices in three-dimensional space.
     */
    public Cuboid(List<ThreeDPoint> vertices) {
        if (vertices.size() != 8)
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getName()));
        int n = 0;
        for (ThreeDPoint p : vertices) this.vertices[n++] = p;
        surfaceArea = (vertices.get(0).coordinates()[0]  - vertices.get(1).coordinates()[0]) * (vertices.get(5).coordinates()[1] - vertices.get(0).coordinates()[1]) * (vertices.get(0).coordinates()[2] - vertices.get(3).coordinates()[2]);
    }

    @Override
    public int compareTo(ThreeDShape t){
        double volThis = this.volume();
        double volThat = t.volume();
        if(volThis > volThat){
            return 1;
        }
        else if(volThis == volThat){
            return 0;
        }
        else{
            return -1;
        }
    }
    @Override
    public double volume() {
        double[] v0 = vertices[0].coordinates();
        double[] v1 = vertices[1].coordinates();
        double[] v3 = vertices[3].coordinates();
        double[] v5 = vertices[5].coordinates();
        double x = 0;
        double y = 0;
        double z = 0;
        for(int i = 0; i < 3; i++){
            x = v0[i] - v1[i];
            if(x != 0){
                i = 3;
            }
        }
        for(int i = 0; i < 3; i++){
            y = v0[i] - v3[i];
            if(y != 0){
                i = 3;
            }
        }
        for(int i = 0; i < 3; i++){
            z = v0[i] - v5[i];
            if(z != 0){
                i = 3;
            }
        }
        if(x != 0 && y != 0 && z != 0){
            double output = x * y * z;
            return output;
        }
        else{
            return 0;
        }
    }
    public double getSurface(){
        double output = surfaceArea;
        return output;
    }
    @Override
    public ThreeDPoint center() {
        double[] v0 = vertices[0].coordinates();
        double[] v7 = vertices[7].coordinates();
        double x = v0[0] + v7[0];
        x = (x/2);
        double y = v0[1] + v7[1];
        y = (y/2);
        double z = v0[2] + v7[2];
        z = (z/2);
        ThreeDPoint output = new ThreeDPoint(x, y, z);
        return output;
    }
    public static Cuboid random(){
        double minX = 0;
        double maxX = 0;
        double minY = 0;
        double maxY = 0;
        double minZ = 0;
        double maxZ = 0;
        ArrayList<ThreeDPoint> cuboid = new ArrayList<ThreeDPoint>();
        minX = Math.floor(Math.random() * 200) - 100;
        maxX = Math.floor(Math.random() * 200) - 100;
        while(maxX < minX){
            maxX = Math.floor(Math.random() * 200) - 100;
        }
        minY = Math.floor(Math.random() * 200) - 100;
        maxY = Math.floor(Math.random() * 200) - 100;
        while(maxY < minY){
            maxY = Math.floor(Math.random() * 200) - 100;
        }
        minZ = Math.floor(Math.random() * 200) - 100;
        maxZ = Math.floor(Math.random() * 200) - 100;
        while(maxZ < minZ){
            maxZ = Math.floor(Math.random() * 200) - 100;
        }
        cuboid.add(new ThreeDPoint(maxX, maxY, maxZ));
        cuboid.add(new ThreeDPoint(maxX, maxY, minZ));
        cuboid.add(new ThreeDPoint(maxX, minY, maxZ));
        cuboid.add(new ThreeDPoint(maxX, minY, minZ));
        cuboid.add(new ThreeDPoint(minX, maxY, maxZ));
        cuboid.add(new ThreeDPoint(minX, maxY, minZ));
        cuboid.add(new ThreeDPoint(minX, minY, maxZ));
        cuboid.add(new ThreeDPoint(minX, minY, minZ));
        Cuboid output = new Cuboid(cuboid);
        return output;
    }
}
