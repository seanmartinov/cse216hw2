package cse216hw2;

import java.util.*;

public class Ordering {

    static class XLocationComparator implements Comparator<TwoDShape> {
        @Override public int compare(TwoDShape o1, TwoDShape o2) {
            int numSide = 0;
            if(o1.numSides() == 4){
                if(o1 instanceof Square){
                    o1 = (Square) o1;
                    numSide += 4;
                }
                else{
                    o1 = (Quadrilateral) o1;
                    numSide += 4;
                }
            }
            else if(o1.numSides() == 2){
                o1 = (Circle) o1;
                numSide += 2;
            }
            if(o2.numSides() == 4){
                if(o2 instanceof Square){
                    o2 = (Square) o2;
                    numSide += 4;
                }
                else{
                    o2 = (Quadrilateral) o2;
                    numSide += 4;
                }
            }
            else if(o2.numSides() == 2){
                o2 = (Circle) o2;
                numSide += 2;
            }
            double[] sides = new double[numSide * 2];
            int counter = 0;
            if(o1.numSides() == 4){
                for(int i = 0; i < o1.numSides(); i++){
                    Quadrilateral temp = (Quadrilateral) o1;
                    double[] t = temp.getPosition().get(i).coordinates();
                    sides[i] = t[0];
                    counter ++;
                }
            }
            else{
                Circle temp = (Circle) o1;
                double[] t = temp.getPosition().get(0).coordinates();
                if(t[0] >= t[1]){
                    sides[counter] = (t[0] + temp.getRadius());
                    counter ++;
                    
                }
                else{
                    sides[counter] = (t[1] + temp.getRadius());
                    counter ++;
                }
            }
            int limit = sides.length;
            double max = 0;
            for(int x = 0; x < limit; x++){
                if(sides[x] > max){
                    max = sides[x];
                }
            }
            if(o2.numSides() == 4){
                for(int i = 0; i < o2.numSides(); i++){
                    Quadrilateral temp = (Quadrilateral) o2;
                    double[] t = temp.getPosition().get(i).coordinates();
                    sides[i + counter] = t[0];
                }
            }
            else{
                Circle temp = (Circle) o2;
                double[] t = temp.getPosition().get(0).coordinates();
                if(t[0] >= t[1]){
                    sides[counter] = (t[0] + temp.getRadius());
                    
                }
                else{
                    sides[counter] = (t[1] + temp.getRadius());
                }
            }
            double secMax = 0;
            for(int x = 0; x < limit; x++){
                if(sides[x] > max){
                    secMax = sides[x];
                }
            }
            if(secMax > max){
                return 1;
            }
            else if(max > secMax){
                return -1;
            }
            else{
                return 0;
            }
        }
    }

    static class AreaComparator implements Comparator<SymmetricTwoDShape> {
        @Override public int compare(SymmetricTwoDShape o1, SymmetricTwoDShape o2) {
            if(o1.area() > o2.area()){
                return -1;
            }
            else if(o2.area() > o1.area()){
                return 1;
            }
            else{
                return 0;
            }
        }
    }

    static class SurfaceAreaComparator implements Comparator<ThreeDShape> {
        @Override public int compare(ThreeDShape o1, ThreeDShape o2){
            if(o1.equals((Cuboid) o1)){
                if(o2.equals((Sphere) o2)){
                    
                }
                else{
                    
                }
            }
            else{
                if(o2.equals((Sphere) o2)){
                    Sphere temp1 = new Sphere
                }
                else{
                    
                }
            }
        }
    }

    // TODO: there's a lot wrong with this method. correct it so that it can work properly with generics.
    static void copy(List<TwoDShape> source, List<TwoDShape> destination) {
        for(int y = destination.size(); y < (destination.size() + source.size()); y++){
            destination.add(source.get(y));
        }
    }
    

    public static void main(String[] args) {
        List<TwoDShape>          shapes          = new ArrayList<>();
        List<SymmetricTwoDShape> symmetricshapes = new ArrayList<>();
        List<ThreeDShape>        threedshapes    = new ArrayList<>();

        /*
         * uncomment the following block and fill in the "..." constructors to create actual instances. If your
         * implementations are correct, then the code should compile and yield the expected results of the various
         * shapes being ordered by their smallest x-coordinate, area, volume, surface area, etc. */

        /*
        symmetricshapes.add(new Rectangle(...));
        symmetricshapes.add(new Square(...));
        symmetricshapes.add(new Circle(...));

        copy(symmetricshapes, shapes); // note-1 //
        shapes.add(new Quadrilateral(new ArrayList<>()));
         */

        // sorting 2d shapes according to various criteria
        shapes.sort(new XLocationComparator());
        symmetricshapes.sort(new XLocationComparator());
        symmetricshapes.sort(new AreaComparator());

        // sorting 3d shapes according to various criteria
        Collections.sort(threedshapes);
        threedshapes.sort(new SurfaceAreaComparator());

        /*
         * if your changes to copy() are correct, uncommenting the following block will also work as expected note that
         * copy() should work for the line commented with 'note-1' while at the same time also working with the lines
         * commented with 'note-2' and 'note-3'. */

        /*
        List<Number> numbers = new ArrayList<>();
        List<Double> doubles = new ArrayList<>();
        Set<Square>        squares = new HashSet<>();
        Set<Quadrilateral> quads   = new LinkedHashSet<>();

        copy(doubles, numbers); // note-2 //
        copy(squares, quads);   // note-3 //
        */
    }
}
