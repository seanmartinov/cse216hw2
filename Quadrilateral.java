package cse216hw2;

import java.util.Arrays;
import java.util.List;

public class Quadrilateral implements Positionable, TwoDShape {

    private final TwoDPoint[] vertices = new TwoDPoint[4];

    public Quadrilateral(double... vertices) { 
        this(TwoDPoint.ofDoubles(vertices));
    }

    public Quadrilateral(List<TwoDPoint> vertices) {
        int n = 0;
        for (TwoDPoint p : vertices) this.vertices[n++] = p;
        if (!isMember(vertices))
            throw new IllegalArgumentException(String.format("Invalid set of vertices specified for %s",
                                                             this.getClass().getCanonicalName()));
    }

    /**
     * Given a list of four points, adds them as the four vertices of this quadrilateral in the order provided in the
     * list. This is expected to be a counterclockwise order of the four corners.
     *
     * @param points the specified list of points.
     * @throws IllegalStateException if the number of vertices provided as input is not equal to four.
     */
    @Override
    public void setPosition(List<? extends Point> points) throws IllegalStateException {
        if(points.size() == 4){
            if(points.get(0) instanceof TwoDPoint){
                for(int i = 0; i < 4; i++){
                    vertices[i] = (TwoDPoint) points.get(i);
                }
            }
            else{
                System.out.println("List not made of TwoDPoints");
            }
        }
        else{
            throw new IllegalStateException("List has Wrong Number of Points (Needs 4)");
        }
    }

    @Override
    public List<TwoDPoint> getPosition() {
        return Arrays.asList(vertices);
    }

    /**
     * @return the lengths of the four sides of the quadrilateral. Since the setter {@link Quadrilateral#setPosition(List)}
     *         expected the corners to be provided in a counterclockwise order, the side lengths are expected to be in
     *         that same order.
     */
    protected double[] getSideLengths() {
        double[] output = new double[4];
        double[] temp = new double[2];
        double[] temp2 = new double[2];
        double diff = 0;
        TwoDPoint first;
        TwoDPoint second;
        for(int i = 0; i < 4; i++){
            first = vertices[i];
            temp = first.coordinates();
            if(i == 3){
                second = vertices[0];
            }
            else{
                second = vertices[i+1];
            }
            temp2 = second.coordinates();
            diff = Math.sqrt(((Math.pow((temp[0] - temp2[0]), 2) + (Math.pow((temp[1] - temp2[1]), 2)))));
            output[i] = diff;
        }
        return output;
    }

    @Override
    public int numSides() { return 4; }

    @Override
    public boolean isMember(List<? extends Point> vertices) { return vertices.size() == 4; }
}
