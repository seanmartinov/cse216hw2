package cse216hw2;
import java.util.List;

public class Rectangle extends Quadrilateral implements SymmetricTwoDShape {
    public Rectangle(List<TwoDPoint> vertices) {
        super(vertices);
        double[] sides = this.getSideLengths();
        if(sides[0] == sides[2] && sides[1] == sides[3]){
            
        }
    }
    /**
     * The center of a rectangle is calculated to be the point of intersection of its diagonals.
     *
     * @return the center of this rectangle.
     */
    @Override
    public Point center() {
        double[] sides = this.getSideLengths();
        TwoDPoint output = new TwoDPoint(sides[0]/2, sides[1]/2);
        return output;
    }

    @Override
    public boolean isMember(List<? extends Point> vertices) {
        if(vertices.size() == 4){
            Quadrilateral test = new Quadrilateral();
            test.setPosition(vertices);
            double[] temp = test.getSideLengths();
            if(temp[0] == temp[1] && temp[0] == temp[2] && temp[0] == temp[3]){
                return true;
            }
            else if(temp[0] == temp[2] && temp[1] == temp[3]){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
    }

    @Override
    public double area() {
        double[] sides = this.getSideLengths();
        double area = sides[0] * sides[1];
        return area;
    }
}
