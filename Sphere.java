package cse216hw2;
import java.util.*;
public class Sphere implements ThreeDShape{
    private double[] cordRad = new double[4];
    private ThreeDPoint center;
    private double surfaceArea;
    public Sphere(double[] input){
        center = new ThreeDPoint(input[0], input[1], input[2]);
        cordRad = input;
        surfaceArea = (4/3) * Math.PI * (Math.pow(input[3], 3));
    }
    public Sphere(ThreeDPoint cen, double rad){
        center = cen;
        double[] temp = cen.coordinates();
        for(int i = 0; i < 3; i++){
            cordRad[i] = temp[i];
        }
        cordRad[3] = rad;
        surfaceArea = (4/3) * Math.PI * (Math.pow(rad, 3));
    }
    @Override
    public Point center(){
        ThreeDPoint output = center;
        return output;
    }
    @Override
    public double volume(){
        double output = (4/3);
        double radCubed = Math.pow(cordRad[3], 3);
        output = output * radCubed * (Math.PI);
        return output;
    }
    @Override
    public int compareTo(ThreeDShape t){
        if(this.volume() > t.volume()){
            return 1;
        }
        else if(this.volume() < t.volume()){
            return -1;
        }
        else{
            return 0;
        }
    }
    public double getSurface(){
        double output = surfaceArea;
        return output;
    }
    public static Sphere random(){
        double centerX = 0;
        double centerY = 0;
        double centerZ = 0;
        double radiusRand = 0;
        centerX = Math.floor(Math.random() * 200) - 100;
        centerY = Math.floor(Math.random() * 200) - 100;
        centerZ = Math.floor(Math.random() * 200) - 100;
        radiusRand = Math.floor(Math.random() * 25);
        ThreeDPoint center = new ThreeDPoint(centerX, centerY, centerZ);
        Sphere output = new Sphere(center, radiusRand);
        return output;
    }
}
