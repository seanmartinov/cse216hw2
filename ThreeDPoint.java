package cse216hw2;
/**
 * An unmodifiable point in the three-dimensional space. The coordinates are specified by exactly three doubles (its
 * <code>x</code>, <code>y</code>, and <code>z</code> values).
 */
public class ThreeDPoint implements Point {
    double[] cords = new double[3];
    public ThreeDPoint(double x, double y, double z) {
        cords[0] = x;
        cords[1] = y;
        cords[2] = z;
    }

    /**
     * @return the (x,y,z) coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
        double[] output = cords;
        return output;
    }
}
