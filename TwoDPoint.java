package cse216hw2;
import java.util.ArrayList;
import java.util.List;

/**
 * An unmodifiable point in the standard two-dimensional Euclidean space. The coordinates of such a point is given by
 * exactly two doubles specifying its <code>x</code> and <code>y</code> values.
 */
public class TwoDPoint implements Point {
    private double[] cords = new double[2];

    public TwoDPoint(double x, double y) {
        cords[0] = x;
        cords[1] = y;
    }
    /**
     * @return the coordinates of this point as a <code>double[]</code>.
     */
    @Override
    public double[] coordinates() {
        double[] output = cords;
        return output;
    }

    /**
     * Returns a list of <code>TwoDPoint</code>s based on the specified array of doubles. A valid argument must always
     * be an even number of doubles so that every pair can be used to form a single <code>TwoDPoint</code> to be added
     * to the returned list of points.
     *
     * @param coordinates the specified array of doubles.
     * @return a list of two-dimensional point objects.
     * @throws IllegalArgumentException if the input array has an odd number of doubles.
     */
    public static List<TwoDPoint> ofDoubles(double... coordinates) throws IllegalArgumentException {
        List<TwoDPoint> output;
        if(coordinates.length % 2 == 0){
            int temp = coordinates.length / 2;
            output = new ArrayList<>();
            for(int i = 0; i < temp; i = i++){
                output.add(new TwoDPoint(coordinates[i], coordinates[i+1]));
                i = i+1;
            }
            return output;
        }
        else{
            throw new IllegalArgumentException("Array has odd number of doubles");
        }
    }
}
